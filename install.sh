mkdir -p /opt/cadvisor

wget -O /opt/cadvisor/docker-compose.yml 'https://gitlab.com/carpla-public/cavisor/-/raw/main/docker-compose.yml'

cd /opt/cadvisor
docker-compose up -d
